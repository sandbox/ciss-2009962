<?php
define('FORMQUERY_METADATA_ID', '#fqid');

require_once dirname(__FILE__) . '/includes/metadata.inc';
require_once dirname(__FILE__) . '/plugins/plugins.inc';
require_once dirname(__FILE__) . '/plugins/selectors.inc';


if (!function_exists('fq')) {
  /**
   * Shorthand function to formquery().
   *
   * To avoid collisions this function will only be set if no other function
   * fq() had been declared.
   *
   * @see formquery()
   *
   * @param $element
   *
   * @return FormQuery
   */
  function fq(&$element) {
    return formquery($element);
  }
}


/**
 * Creates and populates a new FormQuery object.
 *
 * @param array $element
 *  A form element, usually $form
 *
 * @return FormQuery
 */
function formquery(&$element) {
  $instance = new FormQuery();
  $instance->add($element);
  return $instance;
}


/**
 * Retrieves and initializes all plugin definitions.
 *
 * For any key that has not been declared a default value will be set.
 *
 * @return array
 *  An array containing all plugin definitions.
 */
function formquery_get_plugins() {
  $defaults = array(
    // Required: The function or static method to call
    'callback'  => NULL,
    // Optional: Wether the callback will modify the current collection
    // If set to true, the element will receive a new FormQuery instance to populate.
    'filter' => TRUE
  );
  $plugins  = module_invoke_all('formquery_plugins');
  foreach ($plugins as &$plugin) {
    $plugin += $defaults;
  }
  return $plugins;
}


/**
 * Retrieves and initializes all selector definitions.
 *
 * For any key that has not been declared a default value will be set.
 *
 * @return array
 *  An array containing all selector definitions.
 */
function formquery_get_selectors() {
  $defaults = array(
    // Required: The function or static method to call
    'callback'  => NULL,
  );
  $plugins = module_invoke_all('formquery_selectors');
  foreach ($plugins as &$plugin) {
    $plugin += $defaults;
  }
  return $plugins;
}


/**
 * Returns a plugin definition by name.
 *
 * @param string $name
 *  The plugin name.
 * @param bool $validate
 *  If the plugin definition should be verified.
 *
 * @return array
 *  The plugin definition
 * @return bool
 *  If $validate is FALSE and no plugin was found, FALSE will be returned
 * @throws Exception
 *  Thrown if $validate is TRUE and no plugin was found or the callback is not
 *  callable
 */
function formquery_get_plugin($name, $validate = FALSE) {
  static $plugins;
  if (!isset($plugins)) {
    $plugins = & drupal_static('formquery_plugins', formquery_get_plugins());
  }
  if ($validate) {
    if (!isset($plugins[$name])) {
      throw new Exception('Plugin "' . $name . '" not found');
    }

    if (!is_callable($plugins[$name]['callback'])) {
      throw new Exception('Invalid plugin callback for "' . $name . '"');
    }
  }
  elseif (!isset($plugins[$name])) {
    return FALSE;
  }
  return $plugins[$name];
}


/**
 * Checks an element against a selector.
 *
 * @see formquery_match_string()
 * @see formquery_match_array()
 *
 * @param mixed $selector
 * @param array $element
 *
 * @return bool
 */
function formquery_match_element($selector, $element) {
  if (is_null($selector)) {
    return TRUE;
  }
  if (is_string($selector)) {
    return formquery_match_string($selector, $element);
  }
  if (is_array($selector)) {
    return formquery_match_array($selector, $element);
  }
  return FALSE;
}


/**
 * Checks an element against a selector string.
 *
 * @todo Allow plugins to define their own selectors.
 *
 * @param string $selector_string
 * @param array $element
 *
 * @return bool
 */
function formquery_match_string($selector_string, array $element) {
  static $selectors;
  static $plugins;
  if (is_null($selectors)) {
    $plugins   = & drupal_static('formquery_selectors', formquery_get_selectors());
    $selectors = & drupal_static('formquery_parsed_selectors', array());
  }
  if (!isset($selectors[$selector_string])) {
    formquery_parse_selector_string($selector_string, $parsed);
    $selectors[$selector_string] = $parsed;
  }
  foreach ($selectors[$selector_string] as $selector) {
    if (isset($selector['type'])) {
      $element_type = isset($element['#type']) ? $element['#type'] : 'markup';
      if ($element_type !== $selector['type']) {
        return FALSE;
      }
      continue;
    }
    if (isset($selector['plugin'])) {
      $plugin = $selector['plugin'];
      if (!isset($plugins[$plugin]) || !$plugins[$plugin]['callback']($element, $selector['argument'])) {
        return FALSE;
      }
    }
  }
  return TRUE;
}


/**
 * Generates a list of properties/selector plugins from a selector string.
 *
 * @param string $selector
 * @param array $result
 */
function formquery_parse_selector_string($selector, array &$result = array()) {
  $plugins = explode(':', $selector);
  $type = array_shift($plugins);
  if ($type !== '') {
    $result[] = array('type' => $type);
  }
  foreach ($plugins as $plugin) {
    if (preg_match('#^(?<plugin>[^(]+?)(?:\((?<arg>[^)]*?)\))?$#', $plugin, $matches)) {
      $result[] = array('plugin' => $matches['plugin'], 'argument' => $matches['arg']);
    }
  }
}


/**
 * Checks an element against an array.
 *
 * @param array $selector
 * @param array $element
 *
 * @return bool
 */
function formquery_match_array($selector, $element) {
  // @todo Check if this can be refactored into a more concise structure.
  foreach ($selector as $key => $value) {
    $compare_nested = is_array($value);

    // Only compare keys that are associative
    if (is_string($key)) {
      if (!isset($element[$key])) {
        return FALSE;
      }
      if (!$compare_nested) {
        if ($element[$key] !== $value) {
          return FALSE;
        }
      }
      elseif (!formquery_match_array($value, $element[$key])) {
        return FALSE;
      }
    }
    else {
      $element_keys = array_filter(array_keys($element), 'is_int');
      foreach ($element_keys as $element_key) {
        if ($compare_nested) {
          if (formquery_match_array($value, $element[$element_key])) {
            // Skip to the next selector
            continue 2;
          }
        }
        elseif ($element[$element_key] === $value) {
          // Skip to the next selector
          continue 2;
        }
      }
      return FALSE;
    }
  }
  return TRUE;
}


/**
 * Matches a form tree against a selector and stores matched elements.
 *
 * Additional metadata will be set for any traversed element, containing the
 * element name and a reference to its parent element.
 *
 * @param mixed $selector
 * @param array $parent
 * @param array $matches
 *  An array to which all matched elements will be added.
 */
function formquery_find_element($selector, &$parent, &$matches) {
  foreach (element_children($parent) as $name) {
    formquery_set_reference($parent[$name], 'parent', $parent);
    formquery_set_metadata($parent[$name], 'name', $name);
    if (formquery_match_element($selector, $parent[$name])) {
      $matches[] = & $parent[$name];
    }
    formquery_find_element($selector, $parent[$name], $matches);
  }
}


/**
 * Exports an array to valid PHP code.
 * The main difference to var_export() is that the output relies on PHP's
 * implicit array creation to create a compact array representation and
 * thus does not contain any array() statements.
 * TRUE and FALSE will be rendered according to the Drupal CS.
 *
 * @param array $array
 * @param string $prefix
 * @param array  $options
 *
 * @return string
 */
function formquery_dump_array($array, $prefix = '$arr', $options = array()) {
  $paths = array();
  _formquery_flatten_array($array, $paths);

  $options += array(
    // Align indices so that they form visual columns
    'columns_align'       => FALSE,
    // Only align if the difference between column width and key length is not
    // larger than this value. 0 for "always align".
    'columns_align_delta' => 0,
    // Align the last index. Disabling this might increase readability.
    'columns_align_last'  => TRUE,
    // Align equality signs.
    'align_equals'        => FALSE,
    // Do not move equality signs farther than this value. 0 for "any offset".
    'align_equals_max'    => 0,
    // If set, exclude any paths that do not contain this key.
    'filter'              => NULL
  );

  $output = array();

  // If filter option is set, exclude any paths that do not contain the
  // provided filter key.
  if (!is_null($options['filter'])) {
    foreach (array_keys($paths) as $i) {
      $keys = array_slice($paths[$i], 0, -1);
      if (!in_array($options['filter'], $keys, TRUE)) {
        unset($paths[$i]);
      }
    }
  }

  // Convert keys and values to exportable strings.
  foreach ($paths as &$path) {
    $key_count = count($path) - 1;
    // Convert all keys to their output format
    for ($depth = 0; $depth < $key_count; $depth++) {
      $key          = $path[$depth];
      $path[$depth] = is_string($key) || is_null($key) ? "'$key'" : (string) $key;
    }
    // Convert the value into an exportable format.
    $path[$key_count] = is_bool($path[$key_count])
      ? ($path[$key_count] ? 'TRUE' : 'FALSE')
      : var_export($path[$key_count], TRUE);
  }

  // If columns option is set, right-pad each path string so that the
  // keys line up.
  if ($options['columns_align']) {
    $columns = array();
    foreach ($paths as $path) {
      $key_count = count($path) - 1;
      for ($i = 0; $i < $key_count; $i++) {
        $columns[$i] = isset($columns[$i]) ? max($columns[$i], strlen($path[$i])) : strlen($path[$i]);
      }
    }
    foreach ($paths as &$path) {
      $key_count = count($path) - 1;
      for ($i = 0; $i < $key_count; $i++) {
        // Determine if the string should be padded.
        $format = TRUE;
        $format = $format && !$options['columns_align_delta'] || ($columns[$i] - strlen($path[$i]) <= $options['columns_align_delta']);
        $format = $format && ($options['columns_align_last'] || $i + 1 < $key_count);
        if ($format) {
          $path[$i] = str_pad($path[$i], $columns[$i], ' ', STR_PAD_RIGHT);
        }
      }
    }
  }

  // Implode the path in preparation of the following step, alignment of the equality sign.
  foreach ($paths as &$path) {
    $value = array_pop($path);
    $path  = array('[' . implode('][', $path) . ']', $value);
  }
  unset($path);

  // If equals alignment option is set, right-pad each path string so that the
  // assignment operators line up.
  if ($options['align_equals']) {
    $max_length = 0;
    // Retrieve the maximum path length
    foreach ($paths as $path) {
      $max_length = max(strlen($path[0]), $max_length);
    }
    if ($options['align_equals_max']) {
      $max_length = min($options['align_equals_max'], $max_length);
    }
    foreach ($paths as &$path) {
      $path[0] = str_pad($path[0], $max_length, ' ', STR_PAD_RIGHT);
    }
    unset($path);
  }

  foreach ($paths as $path) {
    $output[] = $prefix . $path[0] . ' = ' . $path[1] . ';';
  }
  return implode("\n", $output);
}


/**
 * Generates a flat list of an array's paths.
 * Each entry contains a path's keys and the value as last element.
 *
 * @param array $array
 * @param array $paths
 * @param array $parents
 */
function _formquery_flatten_array(array $array, array &$paths, array $parents = array()) {
  foreach ($array as $key => &$value) {
    $value_parents   = $parents;
    $value_parents[] = $key;
    if (is_array($value)) {
      _formquery_flatten_array($value, $paths, $value_parents);
    }
    else {
      $value_parents[] = $value;
      $paths[]         = $value_parents;
    }
  }
}


/**
 * Generates an exportable array for an element.
 * The element must already have the required metadata.
 *
 * @param array $element
 * @param bool $relative
 * @return array
 */
function formquery_generate_element_dump_array(array $element, $relative = TRUE) {
  $array = $element;
  $parent = $element;
  if (!$relative) {
    do {
      $name = formquery_get_metadata($parent, 'name');
      $parent = formquery_get_metadata($parent, 'parent');
      if (!is_null($name)) {
        $array = array($name => $array);
      }
    } while ($parent);
  }
  return $array;
}


