<?php

class FormQueryClassesTestCase extends DrupalUnitTestCase {

  private static $instance;

  public function getInfo() {
    return array(
      'name' => 'FormQuery classes',
      'description' => 'Tests for FormQuery, FormQueryCollection and plugin arguments',
      'group' => 'FormQuery'
    );
  }


  public function setUp() {
    // @todo Autoloader fails due to missing DB access. Still, there's got
    // to be a better way to load classes ... right?
    module_load_include('inc', 'formquery', 'includes/formquery');
    return parent::setUp();
  }


  public function testFormQueryCollectionAddElements() {

    $collection = new FormQueryCollection();
    $element1 = array('key1' => 'value1');
    $element2 = array('key2' => 'value2');
    $collection->add($element1);
    $collection->add($element2);
    $this->assertEqual(count($collection->get()), 2, 'Collection returns all elements.');

    $collection->add($element1);
    $this->assertEqual(count($collection->get()), 2, 'Elements get added only once.');

    $returned_1 = & $collection->get(0);
    $returned_2 = & $collection->get(1);
    $this->assertIdentical($returned_1['key1'], $element1['key1'], 'First element returned.');
    $this->assertIdentical($returned_2['key2'], $element2['key2'], 'Second element returned.');
    $this->assertNull($collection->get(2), 'NULL returned for undefined offset.');

    $returned_1['new_key1'] = 'new value 1';
    $this->assertIdentical($returned_1['new_key1'], $element1['new_key1'], 'References are maintained on returned element.');

    $returned_elements = $collection->get();
    $returned_elements[1]['new_key2'] = 'new value 2';
    $this->assertIdentical($returned_elements[1]['new_key2'], $element2['new_key2'], 'References are maintained in returned array.');
  }


  public function testFormQueryCollectionAddCollection() {
    $collectionA = new FormQueryCollection();
    $collectionB = new FormQueryCollection();
    $element1 = array('key1' => 'value1');
    $element2 = array('key2' => 'value2');
    $element3 = array('key3' => 'value3');

    $collectionA->add($element1);
    $collectionA->add($element2);

    // Adding $element2 to both collections
    $collectionB->add($element2);
    $collectionB->add($element3);

    $collectionA->add($collectionB);
    $returned_elements = $collectionA->get();
    $this->assertEqual(count($returned_elements), 3, "Collection contains added collection's elements, but no duplicates.");
    $returned_elements[2]['new_key'] = 'new value';
    $this->assertIdentical($returned_elements[2]['new_key'], $element3['new_key'], 'References are maintained in returned array.');
  }


  public function testFormQueryAddElement() {
    // No test for $context, since it currently has no getter and isn't used yet.
    $fquery = new FormQuery();
    $element1 = array('key1' => 'value1');
    $element2 = array('key2' => 'value2');
    $returned_elements = $fquery->add($element1)->add($element2)->get();
    $this->assertEqual(count($returned_elements), 2, 'All elements have been added and returned.');
    $returned_elements[0]['new_key'] = 'new value';
    $this->assertIdentical($returned_elements[0]['new_key'], $element1['new_key'], 'References are maintained in returned array.');
    $this->assertNull($fquery->get(3), 'NULL returned for undefined offset.');
  }


  public function testFormQueryAddFormQuery() {
    $fquery1 = new FormQuery();
    $fquery2 = new FormQuery();

    $element1 = array('key1' => 'value1');
    $element2 = array('key2' => 'value2');
    $element3 = array('key3' => 'value3');

    $fquery1->add($element1)->add($element2);
    $fquery2->add($element2)->add($element3);
    $returned_elements = $fquery1->add($fquery2)->get();
    $this->assertEqual(count($returned_elements), 3, "FormQuery contains all added elements, but no duplicates.");
    $returned_elements[1]['new_key'] = 'new value';
    $this->assertIdentical($returned_elements[1]['new_key'], $element2['new_key'], 'References are maintained in returned array.');
  }


  public function testFormQueryAssign() {
    $fquery1 = new FormQuery();
    $fquery1->assign($fquery2);
    $this->assertIdentical($fquery1, $fquery2, 'Instance has been assigned');
  }


  public function testFormQueryCall() {
    // Plugins will be added during this test
    $plugins = & drupal_static('formquery_plugins');

    $fquery = new FormQuery();
    $defaults = array('filter' => TRUE);

    $plugins['testPluginReceivesOldInstance'] = array(
      'callback' => __CLASS__ . '::callbackPluginTestInstance',
      'filter' => FALSE
    ) + $defaults;
    $instance1 = $fquery->testPluginReceivesOldInstance();
    $this->assertIdentical($instance1, $fquery, 'Same instance returned.');

    $plugins['testPluginReceivesNewInstance'] = array(
      'callback' => __CLASS__ . '::callbackPluginTestInstance',
    ) + $defaults;
    $instance2 = $fquery->testPluginReceivesNewInstance();
    $this->assertNotIdentical($instance2, $fquery, 'New instance returned.');

    $plugins['testPluginArguments'] = array(
      'callback' => __CLASS__ . '::callbackPluginTestArguments',
    ) + $defaults;
    $element = array();
    $fquery->add($element);
    // $result contains the collection and arguments
    $result = $fquery->testPluginArguments(1, 2, 3);
    $this->assertEqual(count($result['collection']->get()), 1, 'Collection contains an element.');
    $this->assertEqual(count($result['arguments']), 3, 'All arguments have been passed.');
  }


  /**
   * Dummy plugin callback.
   *
   * @see FormQueryClassesTestCase::testFormQueryCall()
   */
  public static function callbackPluginTestInstance(FormQuery $instance) {
    return $instance;
  }


  /**
   * Dummy plugin callback.
   *
   * @see FormQueryClassesTestCase::testFormQueryCall()
   */
  public static function callbackPluginTestArguments(FormQuery $instance, FormQueryCollection $collection, array $arguments) {
    return array(
      'collection' => $collection,
      'arguments' => $arguments
    );
  }

}