<?php

class FormQueryMetadataTestCase extends DrupalUnitTestCase {


  public function getInfo() {
    return array(
      'name' => 'FormQuery metadata functions',
      'description' => 'Tests related to setting, storing and retrieving element metadata.',
      'group' => 'FormQuery'
    );
  }


  public function testSetMetadataId() {
    $element = array();

    formquery_set_metadata_id($element);
    $this->assert(array_key_exists(FORMQUERY_METADATA_ID, $element), 'An ID key is added.');
    $this->assertIdentical($element[FORMQUERY_METADATA_ID], 0, 'The first assigned ID is 0.');

    $element[FORMQUERY_METADATA_ID] = NULL;
    formquery_set_metadata_id($element);
    $this->assertNotNull($element[FORMQUERY_METADATA_ID], 'NULL IDs get replaced.');
    $this->assertIdentical($element[FORMQUERY_METADATA_ID], 1, 'IDs get incremented by 1.');

    formquery_set_metadata_id($element);
    $this->assertIdentical($element[FORMQUERY_METADATA_ID], 1, 'An existing ID will not be replaced.');

    drupal_static_reset('formquery_metadata_next_key');
    $element = array();
    formquery_set_metadata_id($element);
    $this->assertIdentical($element[FORMQUERY_METADATA_ID], 0, 'The internal index can be reset.');
  }


  public function testSetMetadataInternal() {
    $element = array();

    // Assign by value
    $value = 'val';
    _formquery_set_metadata($element, 'meta_value', $value);
    $this->assertIdentical($element[FORMQUERY_METADATA_ID], 0, 'Setting data assigns an ID.');
    $metadata = drupal_static('formquery_metadata');
    $this->assertIdentical($metadata[0]['meta_value'], $value, 'A value is added.');

    // Assign by reference
    $ref = array();
    _formquery_set_metadata($element, 'meta_ref', NULL, $ref, TRUE);
    $ref_value = 'ref val';
    $ref['new_key'] = $ref_value;
    $metadata = drupal_static('formquery_metadata');
    $this->assertIdentical($metadata[0]['meta_ref']['new_key'], $ref_value, 'A reference is added.');
  }


  public function testGetMetadataInternal() {
    $element = array();
    $this->assertNull(_formquery_get_metadata($element, 'meta_value'), 'NULL returned for an element without metadata');

    // Return by value
    $value = 'meta value';
    _formquery_set_metadata($element, 'meta_value', $value);
    $this->assertIdentical(_formquery_get_metadata($element, 'meta_value'), $value, 'A stored value is returned');

    // Return by reference
    $referenced = array();
    _formquery_set_metadata($element, 'meta_ref', NULL, $referenced, TRUE);
    $ref_return = & _formquery_get_metadata($element, 'meta_ref', TRUE);
    $ref_return['new_key'] = 'new value';
    $this->assertIdentical($ref_return['new_key'], $referenced['new_key'], 'References are kept intact.');

    $this->assertNull(_formquery_get_metadata($element, 'does_not_exist'), 'NULL returned if key not set.');
  }


  public function testSetMetadata() {
    $element = array();
    formquery_set_metadata($element, 'test_key', 'test value');
    $this->assertIdentical(_formquery_get_metadata($element, 'test_key'), 'test value', 'A value is set.');
  }


  public function testSetReference() {
    $element = array();
    $referenced = array();
    formquery_set_reference($element, 'test_key', $referenced);
    $referenced['new_key'] = 'new value';
    // No need to return by reference for this test
    $ref_return = _formquery_get_metadata($element, 'test_key');
    $this->assertIdentical($referenced['new_key'], $ref_return['new_key'], 'A reference is set.');
  }


  public function testGetMetadata() {
    $element = array();
    _formquery_set_metadata($element, 'test_key', 'test value');
    $this->assertIdentical(formquery_get_metadata($element, 'test_key'), 'test value', 'A value is returned.');
  }


  public function testGetReference() {
    $element = array();
    $referenced = array();
    _formquery_set_metadata($element, 'meta_ref', NULL, $referenced, TRUE);
    $ref_return = & formquery_get_reference($element, 'meta_ref');
    $ref_return['new_key'] = 'new value';
    $this->assertIdentical($ref_return['new_key'], $referenced['new_key'], 'References are kept intact.');
  }



}