<?php

/**
 * Implements hook_formquery_plugins()
 */
function formquery_formquery_plugins() {

  $plugins = array();

  // Traversing and filtering

  // @method bool is() is(mixed $selector,...)
  $plugins['is'] = array('callback' => 'formquery_plugin_is', 'filter' => FALSE);

  // @method FormQuery find() find(mixed $selector,...)
  $plugins['find'] = array('callback' => 'formquery_plugin_find');

  // @method FormQuery filter() filter(mixed $selector,...)
  $plugins['filter'] = array('callback' => 'formquery_plugin_filter');

  // @method FormQuery parent() parent(mixed $selector,...)
  $plugins['parent'] = array('callback' => 'formquery_plugin_parent');

  // @method FormQuery children() children(mixed $selector,...)
  $plugins['children'] = array('callback' => 'formquery_plugin_children');

  // @method FormQuery prev() prev(mixed $selector,...)
  $plugins['prev'] = array('callback' => 'formquery_plugin_prev');

  // @method FormQuery prevAll() prevAll(mixed $selector,...)
  $plugins['prevAll'] = array('callback' => 'formquery_plugin_prev_all');

  // @method FormQuery next() next(mixed $selector,...)
  $plugins['next'] = array('callback' => 'formquery_plugin_next');

  // @method FormQuery nextAll() nextAll(mixed $selector,...)
  $plugins['nextAll'] = array('callback' => 'formquery_plugin_next_all');

  // Manipulation: values

  // @method bool hasClass() hasClass(string $class)
  $plugins['hasClass'] = array('callback' => 'formquery_plugin_has_class', 'filter' => FALSE);

  // @method FormQuery addClass() addClass(string $class,...)
  $plugins['addClass'] = array('callback' => 'formquery_plugin_add_class', 'filter' => FALSE);

  // @method FormQuery removeClass() removeClass(string $class,...)
  $plugins['removeClass'] = array('callback' => 'formquery_plugin_remove_class', 'filter' => FALSE);

  // Development
  // Note: These functions output data via a callback. This callback can be set with formquery_output_func()
  // Examples: formquery_output_func('var_dump'), formquery_output_func('dpm')

  // @method FormQuery time() time(string $label = 'Timer')
  // Use time(null) to supress output, e.g. on the first call
  $plugins['time'] = array('callback' => 'formquery_plugin_time', 'filter' => FALSE);

  // @method FormQuery mem() mem(string $label = 'Memory')
  // Use mem(null) to supress output, e.g. on the first call
  $plugins['mem'] = array('callback' => 'formquery_plugin_mem', 'filter' => FALSE);

  // @method FormQuery export() export(string $form_var = 'form', bool $relative = false, array $options = array())
  // For available options @see formquery_dump_array()
  $plugins['export'] = array('callback' => 'formquery_plugin_export', 'filter' => FALSE);

  return $plugins;
}

require_once dirname(__FILE__) . '/plugins.traversing.inc';
require_once dirname(__FILE__) . '/plugins.manipulation.inc';
require_once dirname(__FILE__) . '/plugins.dev.inc';

