<?php



function formquery_plugin_export(FormQuery $instance, FormQueryCollection $collection, $arguments) {
  list($form_var, $relative, $options) = $arguments + array('form', FALSE, array());
  $output = array();
  foreach ($collection->get() as $element) {
    $array = formquery_generate_element_dump_array($element, $relative);
    $output[] = formquery_dump_array($array, '$' . $form_var, $options);
  }
  $output = implode("\n", $output);
  call_user_func(formquery_output_func(), $output);
  return $instance;
}


function formquery_plugin_time(FormQuery $instance, FormQueryCollection $collection, $arguments) {
  static $last;
  list($label) = $arguments + array('Time');
  $time = microtime(TRUE);
  if (is_null($last)) {
    $last = $time;
  }
  if (!is_null($label)) {
    call_user_func(formquery_output_func(), "$label: " . number_format($time - $last, 5) . ' s');
  }
  $last = $time;
  return $instance;
}


function formquery_plugin_mem(FormQuery $instance, FormQueryCollection $collection, $arguments) {
  static $last;
  list($label) = $arguments + array('Memory');
  $mem = memory_get_usage();
  if (is_null($last)) {
    $last = $mem;
  }
  if (!is_null($label)) {
    call_user_func(formquery_output_func(), "$label: " . number_format($mem - $last, 0) . ' B');
  }
  $last = $mem;
  return $instance;
}


function formquery_output_func($func = NULL, $temporary = FALSE) {
  static $callback;
  if (!is_null($func)) {
    $callback = $func;
    if (!$temporary) {
      variable_set('formquery_output_func', $func);
    }
  }
  elseif (is_null($callback)) {
    $callback = variable_get('formquery_output_func', 'print_r');
  }
  return $callback;
}
