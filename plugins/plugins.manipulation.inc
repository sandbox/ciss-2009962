<?php

function formquery_plugin_has_class(FormQuery $instance, FormQueryCollection $collection, $arguments) {
  $class   = $arguments[0];
  $element = $collection->get(0);
  return $element && isset($element['#attributes']['class']) && in_array($class, $element['#attributes']['class']);
}


function formquery_plugin_add_class(FormQuery $instance, FormQueryCollection $collection, $classes) {
  $classes  = array_filter(explode(' ', implode(' ', $classes)), 'strlen');
  $elements = & $collection->get();
  foreach ($elements as &$element) {
    if (!isset($element['#attributes']['class'])) {
      $element['#attributes']['class'] = $classes;
    }
    else {
      $element['#attributes']['class'] = array_merge($element['#attributes']['class'], $classes);
    }
  }
  return $instance;
}


function formquery_plugin_remove_class(FormQuery $instance, FormQueryCollection $collection, $classes) {
  $classes  = array_filter(explode(' ', implode(' ', $classes)), 'strlen');
  $elements = & $collection->get();
  foreach ($elements as &$element) {
    if (!empty($element['#attributes']['class'])) {
      $element['#attributes']['class'] = array_diff($element['#attributes']['class'], $classes);
    }
  }
  return $instance;
}


