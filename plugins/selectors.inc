<?php

/**
 * Implements hook_formquery_selectors().
 */
function formquery_formquery_selectors() {
  $selectors = array();
  $selectors['name'] = array(
    'callback' => 'formquery_selector_name'
  );
  return $selectors;
}

function formquery_selector_name($element, $name) {
  return !is_null($name) && $name === formquery_get_metadata($element, 'name');
}

