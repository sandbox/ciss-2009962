<?php

function formquery_plugin_is(FormQuery $instance, FormQueryCollection $collection, $selectors) {
  foreach ($collection->get() as $element) {
    foreach ($selectors as $selector) {
      if (formquery_match_element($selector, $element)) {
        return TRUE;
      }
    }
  }
  return FALSE;
}


function formquery_plugin_find(FormQuery $instance, FormQueryCollection $collection, $selectors) {
  $elements = & $collection->get();
  $matches  = array();

  foreach ($selectors as $selector) {
    foreach ($elements as &$element) {
      formquery_find_element($selector, $element, $matches);
    }
  }

  foreach (array_keys($matches) as $index) {
    $instance->add($matches[$index]);
  }
  return $instance;
}


function formquery_plugin_filter(FormQuery $instance, FormQueryCollection $collection, $selectors) {
  $elements = & $collection->get();

  foreach (array_keys($elements) as $index) {
    foreach ($selectors as $selector) {
      if (formquery_match_element($selector, $elements[$index])) {
        $instance->add($elements[$index]);
        break;
      }
    }
  }
  return $instance;
}


function formquery_plugin_parent(FormQuery $instance, FormQueryCollection $collection, $selectors) {
  $elements = & $collection->get();

  foreach ($elements as &$element) {
    $parent = & formquery_get_reference($element, 'parent');
    _formquery_plugin_add_element($instance, $parent, $selectors);
  }
  return $instance;
}


function formquery_plugin_children(FormQuery $instance, FormQueryCollection $collection, $selectors) {
  $elements = & $collection->get();

  foreach ($elements as &$element) {
    $parent = & formquery_get_reference($element, 'parent');
    foreach (element_children($parent, TRUE) as $child) {
      _formquery_plugin_add_element($instance, $parent[$child], $selectors);
    }
  }
  return $instance;
}


function formquery_plugin_prev(FormQuery $instance, FormQueryCollection $collection, $selectors) {
  $elements = & $collection->get();
  foreach ($elements as &$element) {
    $siblings = & _formquery_plugin_get_siblings($element, 'prev');
    foreach ($siblings as &$sibling) {
      _formquery_plugin_add_element($instance, $sibling, $selectors);
    }
  }
  return $instance;
}


function formquery_plugin_next(FormQuery $instance, FormQueryCollection $collection, $selectors) {
  $elements = & $collection->get();
  foreach ($elements as &$element) {
    $siblings = & _formquery_plugin_get_siblings($element, 'next');
    foreach ($siblings as &$sibling) {
      _formquery_plugin_add_element($instance, $sibling, $selectors);
    }
  }
  return $instance;
}


function formquery_plugin_prev_all(FormQuery $instance, FormQueryCollection $collection, $selectors) {
  $elements = & $collection->get();
  foreach ($elements as &$element) {
    $siblings = & _formquery_plugin_get_siblings($element, 'prevAll');
    foreach ($siblings as &$sibling) {
      _formquery_plugin_add_element($instance, $sibling, $selectors);
    }
  }
  return $instance;
}


function formquery_plugin_next_all(FormQuery $instance, FormQueryCollection $collection, $selectors) {
  $elements = & $collection->get();
  foreach ($elements as &$element) {
    $siblings = & _formquery_plugin_get_siblings($element, 'nextAll');
    foreach ($siblings as &$sibling) {
      _formquery_plugin_add_element($instance, $sibling, $selectors);
    }
  }
  return $instance;
}


function &_formquery_plugin_get_siblings($element, $mode) {
  $name     = formquery_get_metadata($element, 'name');
  $parent   = & formquery_get_reference($element, 'parent');
  $siblings = element_children($parent, TRUE);
  // TODO: Might need some error recovery
  $offset = array_search($name, $siblings);
  switch ($mode) {
    case 'prev':
      $selection = $offset ? array($siblings[$offset - 1]) : array();
      break;

    case 'next':
      $selection = $offset < count($siblings) - 1 ? array($siblings[$offset + 1]) : array();
      break;

    case 'prevAll':
      $selection = array_slice($siblings, 0, $offset);
      break;

    case 'nextAll':
      $selection = array_slice($siblings, $offset + 1);
      break;

    default:
      $selection = array();
  }
  $elements = array();
  foreach ($selection as $child) {
    $elements[] = & $parent[$child];
  }
  return $elements;
}


function _formquery_plugin_add_element(FormQuery $instance, &$element, $selectors) {
  if (!$selectors) {
    $instance->add($element);
    return;
  }
  foreach ($selectors as $selector) {
    if (formquery_match_element($selector, $element)) {
      $instance->add($element);
      return;
    }
  }
}
