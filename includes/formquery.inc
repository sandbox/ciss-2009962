<?php


class FormQuery {

  protected $collection;


  public function __construct() {
    $this->collection = new FormQueryCollection();
  }


  /**
   * Adds an element or collection.
   *
   * @param array|FormQueryCollection $element
   *
   * @return $this
   */
  public function add(&$element) {
    if ($element instanceof FormQuery) {
      $this->add($element->collection);
    }
    else {
      $this->collection->add($element);
    }
    return $this;
  }


  /**
   * Proxy method to FormQueryCollection::get()
   *
   * @see FormQueryCollection::get
   *
   * @param int $index Optional
   *  Index of the element that should be returned
   *
   * @return array
   *  If no $index has been given, an array containing references
   *  to the collection's elements
   *  If $index has been given, a reference to collection element
   * @return null
   *  If $index was given but did not match an element
   */
  public function &get($index = NULL) {
    return $this->collection->get($index);
  }


  /**
   * Syntactic sugar to assign instances to variables.
   *
   * This method can be used to assign the current instance to $var without
   * interrupting a chain. Any previous variable content will be overwritten.
   *
   * @param null $var
   *
   * @return $this
   */
  public function assign(&$var) {
    $var = $this;
    return $this;
  }


  protected function getChainable() {
    $instance = new self();
    return $instance;
  }


  public function __call($name, $args) {
    $plugin   = formquery_get_plugin($name, TRUE);
    $instance = $plugin['filter'] ? $this->getChainable() : $this;
    return call_user_func($plugin['callback'], $instance, $this->collection, $args);
  }
}


class FormQueryCollection {

  private $elements = array();

  private $ids = array();

  public function add(&$element) {
    if ($element instanceof FormQueryCollection) {
      $elements = & $element->get();
      foreach ($elements as &$collection_element) {
        $this->add($collection_element);
      }
      return;
    }
    if (!isset($element[FORMQUERY_METADATA_ID])) {
      formquery_set_metadata_id($element);
    }
    if (!isset($this->ids[$element[FORMQUERY_METADATA_ID]])) {
      $this->ids[$element[FORMQUERY_METADATA_ID]] = TRUE;
      $this->elements[]                           = & $element;
    }
  }


  public function &get($index = NULL) {
    if (is_null($index)) {
      return $this->elements;
    }
    if (isset($this->elements[$index])) {
      return $this->elements[$index];
    }
    // NULL cannot be returned by reference
    $return = NULL;
    return $return;
  }
}
