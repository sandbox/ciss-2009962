<?php

/**
 * Sets element metadata by reference.
 *
 * @param array $element
 * @param string $key
 * @param mixed $ref
 */
function formquery_set_reference(&$element, $key, &$ref) {
  _formquery_set_metadata($element, $key, NULL, $ref, TRUE);
}

/**
 * Sets element metadata.
 *
 * @param array $element
 * @param string $key
 * @param mixed $value
 */
function formquery_set_metadata(&$element, $key, $value) {
  _formquery_set_metadata($element, $key, $value);
}

/**
 * Initializes the metadata ID on an element.
 *
 * @param array $element
 *  A form element
 */
function formquery_set_metadata_id(&$element) {
  static $next_key;
  if (is_null($next_key)) {
    $next_key = & drupal_static('formquery_metadata_next_key', 0);
  }
  if (!isset($element[FORMQUERY_METADATA_ID])) {
    $element[FORMQUERY_METADATA_ID] = $next_key++;
  }
}


/**
 * Sets element metadata by value or reference.
 *
 * @param array $element
 * @param string $key
 * @param mixed $value
 * @param mixed $ref Optional
 * @param bool $use_ref Optional
 */
function _formquery_set_metadata(&$element, $key, $value = NULL, &$ref = NULL, $use_ref = FALSE) {
  // TODO: Check how form caching might interfere.
  static $metadata;
  if (is_null($metadata)) {
    $metadata = & drupal_static('formquery_metadata', array());
  }
  formquery_set_metadata_id($element);
  if ($use_ref) {
    $metadata[$element[FORMQUERY_METADATA_ID]][$key] = & $ref;
  }
  else {
    $metadata[$element[FORMQUERY_METADATA_ID]][$key] = $value;
  }
}


/**
 * Returns element metadata.
 *
 * @param array $element
 * @param string $key
 *
 * @return mixed
 */
function formquery_get_metadata($element, $key) {
  $value = _formquery_get_metadata($element, $key);
  return $value;
}


/**
 * Returns element metadata by reference.
 *
 * @param array $element
 * @param string $key
 *
 * @return mixed
 */
function &formquery_get_reference($element, $key) {
  $value = & _formquery_get_metadata($element, $key, TRUE);
  return $value;
}


/**
 * Returns element metadata by value or reference.
 *
 * @param array $element
 * @param string $key
 * @param bool $use_ref
 *
 * @return mixed
 * @return null
 */
function &_formquery_get_metadata($element, $key, $use_ref = FALSE) {
  // @todo Check how form caching might interfere.
  static $metadata;
  if (is_null($metadata)) {
    $metadata = & drupal_static('formquery_metadata', array());
  }
  $id = isset($element[FORMQUERY_METADATA_ID]) ? $element[FORMQUERY_METADATA_ID] : NULL;
  if (!is_null($id) && isset($metadata[$id][$key])) {
    if ($use_ref) {
      $value = & $metadata[$id][$key];
    }
    else {
      $value = $metadata[$id][$key];
    }
  }
  else {
    $value = NULL;
  }
  return $value;
}
